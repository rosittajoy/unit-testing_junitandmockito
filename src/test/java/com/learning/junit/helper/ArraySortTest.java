package com.learning.junit.helper;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class ArraySortTest {

	@Test
	public void testArraySort() {
		int[] unsorted = { 2, 4, 6, 1, 3, 5 };
		int[] sorted = { 1, 2, 3, 4, 5, 6 };
		Arrays.sort(unsorted);
		assertArrayEquals(unsorted, sorted);
	}

	@Test
	public void testArraySortException() {
		int[] unsorted = { 2, 4, 6, 1, 3, 5 };
		int[] sorted = { 1, 2, 3, 4, 5, 6 };
		Arrays.sort(unsorted);
		assertArrayEquals(unsorted, sorted);
	}
}
