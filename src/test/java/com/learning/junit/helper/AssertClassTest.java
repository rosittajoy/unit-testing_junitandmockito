package com.learning.junit.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AssertClassTest {

	StringHelper helper = new StringHelper();

	@Test
	public void testTruncateAInFirst2Positions() {

		assertEquals(helper.truncateAInFirst2Positions("AACD"), "CD");
	}

	@Test
	public void testAreFirstAndLastTwoCharactersTheSame() {
		assertEquals(helper.areFirstAndLastTwoCharactersTheSame("CDCD"), true);
		assertFalse(helper.areFirstAndLastTwoCharactersTheSame("CDDD"));
		assertTrue(helper.areFirstAndLastTwoCharactersTheSame("CDDD"));
		assertNull(null);
		assertNotNull(helper);
	}

}
