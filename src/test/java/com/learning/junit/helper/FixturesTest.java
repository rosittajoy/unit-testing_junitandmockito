package com.learning.junit.helper;

import static org.junit.Assert.assertTrue;

public class FixturesTest {
	protected int value1, value2;

	// assigning the values
	protected void setUp() {
		value1 = 3;
		value2 = 3;
	}

	// test method to add two values
	public void testAdd() {
		double result = value1 + value2;
		assertTrue(result == 6);
	}

	protected void tearDown() {
		value1 = 3;
		value2 = 3;
		assertTrue(value1 + value2 == 6);
	}
}
