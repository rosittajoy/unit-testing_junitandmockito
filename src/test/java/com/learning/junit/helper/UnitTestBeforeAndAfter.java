package com.learning.junit.helper;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UnitTestBeforeAndAfter {
	StringHelper helper = new StringHelper();

	@BeforeClass
	public static void testBeforeClass() {
		System.out.println("BeforeClass Execute before test execute");
	}

	@Before
	public void testBefore() {
		System.out.println("Execute before test execute");
	}

	@Test
	public void testBeforeAndAfter() {
		assertEquals(helper.truncateAInFirst2Positions("AACD"), "AA");
		System.out.println("Test Executed");
	}

	@After
	public void testAfter() {
		System.out.println("Execute After test execute");
	}

	@AfterClass
	public static void testAfterClass() {
		System.out.println("AfterClass Execute after test execute");
	}
}
