package com.learning.mockito.Stack;

import com.learning.mockito.Stock.Stock;

public interface StockService {
	public double getPrice(Stock stock);
}
